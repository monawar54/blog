-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table blog.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.categories: ~5 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(2, 'Python', '2018-05-20 07:32:11', '2018-05-20 11:01:20'),
	(3, 'Laravel', '2018-05-20 11:01:10', '2018-05-20 11:01:10'),
	(4, 'Tutorials', '2018-05-20 11:01:31', '2018-05-20 11:01:31'),
	(5, 'Blog', '2018-05-20 11:01:42', '2018-05-20 11:01:42'),
	(6, 'about', '2018-05-20 11:02:24', '2018-05-20 11:02:24');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table blog.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.migrations: ~10 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(9, '2018_05_15_095826_create_tags_table', 2),
	(12, '2014_10_12_000000_create_users_table', 3),
	(13, '2014_10_12_100000_create_password_resets_table', 3),
	(14, '2018_05_10_092135_create_posts_table', 3),
	(15, '2018_05_10_092720_create_categories_table', 3),
	(16, '2018_05_15_100014_create_post_tag_table', 3),
	(17, '2018_05_15_110706_create_tags_table', 3),
	(18, '2018_05_19_094240_create_profiles_table', 3),
	(19, '2018_05_20_093034_create_settings_table', 4),
	(20, '2018_05_22_110337_insert_user_id_column', 5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table blog.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table blog.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `featured` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.posts: ~18 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `category_id`, `featured`, `deleted_at`, `created_at`, `updated_at`, `user_id`) VALUES
	(1, 'Installing laravel for windows 10', 'installing-laravel-for-windows-10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eleifend aliquam sem, vel vestibulum turpis viverra vel. Quisque lobortis tortor ut nibh luctus consequat. Suspendisse elit elit, hendrerit vitae fringilla ac, ullamcorper vel enim. Nullam quis libero imperdiet, malesuada nisi non, viverra mauris. Duis placerat', 2, '1526816187videogg.jpg', NULL, '2018-05-20 11:36:27', '2018-05-20 11:38:24', 7),
	(2, 'This is my first blog post yet here', 'this-is-my-first-blog-post', 'Suspendisse elit elit, hendrerit vitae fringilla ac, ullamcorper vel enim. Nullam quis libero imperdiet, malesuada nisi non, viverra mauris. Duis placerat mi elit. Aenean placerat massa sed mauris rhoncus placerat. Mauris auctor aliquet felis, scelerisque tincidunt velit consequat quis. Sed non vulputate metus, vitae vehicula enim. Vestibulum viverra quis felis fringilla accumsan. Pellentesque vitae rutrum purus, et mattis sapien. Sed varius', 2, '1526816816videoaa.jpg', NULL, '2018-05-20 11:46:56', '2018-05-22 07:33:05', 8),
	(3, 'My first tutorial in laravel 5.6', 'my-first-tutorial-in-laravel-56', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\nDonec bibendum diam ac lacus varius, quis volutpat augue interdum.\r\nSed pharetra magna nec ultricies gravida.\r\nEtiam vel odio fringilla, efficitur felis in, aliquet nunc.\r\nDuis porttitor sapien quis ex dapibus congue.\r\nDonec aliquet eros id dui vulputate eleifend.\r\nMauris volutpat mauris a dapibus cursus.\r\nSed quis ligula cursus, interdum ante id, malesuada lorem.\r\nVivamus vulputate magna et erat sollicitudin hendrerit.\r\nMauris ultricies erat et lobortis maximus.\r\nFusce dignissim lacus in tristique euismod.\r\nCurabitur vulputate lectus a risus interdum accumsan.\r\nDonec eleifend orci et nulla dictum, at accumsan velit euismod.\r\nDuis finibus quam vel auctor rhoncus.\r\nMauris eu leo tempor, ultricies risus vitae, elementum massa.\r\nDonec laoreet mi volutpat felis varius luctus.', 4, '15268802101eco.jpg', NULL, '2018-05-21 05:23:30', '2018-05-21 05:23:30', 7),
	(4, 'Laravel with vuejs in action for those', 'laravel-with-vuejs-in-action', 'Nam sagittis hendrerit erat, at euismod nibh. Integer et urna vitae dui sodales consectetur. In semper tempor laoreet. Aliquam quis justo vulputate, sagittis sem non, mattis justo. Nam dui ipsum, tempor quis aliquam rhoncus, vestibulum tempus dolor. Mauris ante quam, fermentum efficitur dui vitae, congue lacinia neque. Phasellus congue scelerisque ante nec rutrum. Etiam bibendum eros turpis, a placerat lorem placerat ut. Vestibulum leo nunc, sollicitudin eu tincidunt ut, gravida id lacus. Nunc luctus rhoncus tortor, ut vehicula turpis congue quis. Morbi scelerisque mauris a turpis placerat, sed fringilla ante ullamcorper. Duis id gravida ante, non venenatis orci. Integer et cursus tortor. Fusce vitae mi cursus, ultricies neque ac, condimentum sapien. Cras elit leo, dapibus a ex non, venenatis semper urna.\r\n\r\nPhasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.', 2, '1526880301economy 2.jpg', NULL, '2018-05-21 05:25:01', '2018-05-22 07:34:05', 7),
	(5, 'First Tutorial post in my blog', 'first-tutorial-post-in-my-blog', 'Nam sagittis hendrerit erat, at euismod nibh. Integer et urna vitae dui sodales consectetur. In semper tempor laoreet. Aliquam quis justo vulputate, sagittis sem non, mattis justo. Nam dui ipsum, tempor quis aliquam rhoncus, vestibulum tempus dolor. Mauris ante quam, fermentum efficitur dui vitae, congue lacinia neque. Phasellus congue scelerisque ante nec rutrum. Etiam bibendum eros turpis, a placerat lorem placerat ut. Vestibulum leo nunc, sollicitudin eu tincidunt ut, gravida id lacus. Nunc luctus rhoncus tortor, ut vehicula turpis congue quis. Morbi scelerisque mauris a turpis placerat, sed fringilla ante ullamcorper. Duis id gravida ante, non venenatis orci. Integer et cursus tortor. Fusce vitae mi cursus, ultricies neque ac, condimentum sapien. Cras elit leo, dapibus a ex non, venenatis semper urna.\r\n\r\nPhasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.', 4, '1526883218computer-vision.jpg', NULL, '2018-05-21 06:13:38', '2018-05-21 06:13:38', 7),
	(6, 'Laravel tutorial post for first time', 'laravel-tutorial-post-for-first-time', 'Phasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum,', 3, '1526884027Music-.jpg', NULL, '2018-05-21 06:27:07', '2018-05-21 06:27:07', 7),
	(7, 'photography is the market place for entertainer', 'photography-is-the-best-market-place-for-entertainment', 'Phasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum,', 5, '1526884571fasho.jpg', NULL, '2018-05-21 06:36:11', '2018-05-21 06:37:20', 7),
	(8, 'elementum lacus felis id urna. Morbi', 'elementum-lacus-felis-id-urna-morbi-sagittis-dignissim', 'rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum,', 3, '1526886147Finance-1.jpg', NULL, '2018-05-21 07:02:27', '2018-05-22 12:10:06', 7),
	(9, 'posuere facilisis euismod. Phasellus suscipit', 'posuere-facilisis-euismod-phasellus-suscipit', 'Phasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum,', 3, '1526886289music w.jpg', NULL, '2018-05-21 07:04:49', '2018-05-21 07:04:49', 7),
	(10, 'suscipit posuere facilisis euismod. Phasellus', 'suscipit-posuere-facilisis-euismod-phasellus', 'Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum, Phasellus odio neque, rhoncus id accumsan id, elementum id justo.', 3, '1526886921sports1.jpg', NULL, '2018-05-21 07:05:41', '2018-05-21 07:15:21', 8),
	(11, 'Quam non ipsum euismod euismod ac eget nisi.', 'quam-non-ipsum-euismod-euismod-ac-eget-nisi', 'Non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi. non ipsum euismod euismod ac eget nisi.', 2, '1526966438group_outdoors2.jpg', NULL, '2018-05-22 05:20:38', '2018-05-22 05:20:38', 8),
	(12, 'facilisis euismod. Phasellus posuere facilisis euismo.', 'facilisis-euismod-phasellus-posuere-facilisis-euismo', 'posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit posuere facilisis euismod. Phasellus suscipit', 2, '1526966842Sports-I.jpg', NULL, '2018-05-22 05:27:22', '2018-05-22 07:17:43', 8),
	(13, 'efwef height efwef height efwef height efwef', 'efwef', 'efwef', 3, '15269698001vd.jpg', NULL, '2018-05-22 06:16:40', '2018-05-22 08:09:06', 8),
	(14, 'eatejatha  tyhaeryhaeryh  tyhaeryhaeryh  tyhaery', 'eatejatha', 'tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh  tyhaeryhaeryh tyhaeryhaeryh  tyhaeryhaeryh', 3, '1526969813a57b888517131f8b5188a69e1f0d0c2d.jpg', NULL, '2018-05-22 06:16:53', '2018-05-22 12:07:15', 8),
	(15, 'EQGRYKS tortor eget, volutpat bibendum nisl', 'eqgryks', 'Suspendisse mattis magna a mauris tempus molestie. Nunc posuere sollicitudin lectus ut sagittis. Sed eget orci quis nunc aliquet consectetur. Sed tempus quam et ex placerat lobortis. Maecenas porttitor orci quis lorem ornare pharetra. Proin sit amet tellus tellus. Phasellus dignissim tempor vestibulum. Curabitur nec ultricies purus. Donec condimentum est non ornare ullamcorper. Proin porttitor luctus urna sit amet rhoncus. Vestibulum molestie sagittis erat, eu pellentesque nulla consectetur a. Proin a sollicitudin quam. Donec pharetra nibh eget hendrerit consectetur. Nunc ac iaculis justo, eu aliquam ex. Nullam quis mollis nisl. Proin non sagittis massa.\r\n\r\nEtiam id lobortis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ex massa, dictum non posuere id, congue in libero. Integer pretium sed justo id lobortis. Praesent elit orci, egestas id tortor eget, volutpat bibendum nisl. Nullam nec fringilla elit. Pellentesque cursus suscipit massa sit amet mattis. Pellentesque vitae mauris sollicitudin, faucibus', 3, '1526969825Business-Management.jpg', NULL, '2018-05-22 06:17:06', '2018-05-22 12:08:10', 8),
	(16, 'Suspendisse mattis magna a mauris tempus molestie', 'evqeva', 'Suspendisse mattis magna a mauris tempus molestie. Nunc posuere sollicitudin lectus ut sagittis. Sed eget orci quis nunc aliquet consectetur. Sed tempus quam et ex placerat lobortis. Maecenas porttitor orci quis lorem ornare pharetra. Proin sit amet tellus tellus. Phasellus dignissim tempor vestibulum. Curabitur nec ultricies purus. Donec condimentum est non ornare ullamcorper. Proin porttitor luctus urna sit amet rhoncus. Vestibulum molestie sagittis erat, eu pellentesque nulla consectetur a. Proin a sollicitudin quam. Donec pharetra nibh eget hendrerit consectetur. Nunc ac iaculis justo, eu aliquam ex. Nullam quis mollis nisl. Proin non sagittis massa.\r\n\r\nEtiam id lobortis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ex massa, dictum non posuere id, congue in libero. Integer pretium sed justo id lobortis. Praesent elit orci, egestas id tortor eget, volutpat bibendum nisl. Nullam nec fringilla elit. Pellentesque cursus suscipit massa sit amet mattis. Pellentesque vitae mauris sollicitudin, faucibus', 3, '1526969841fash.jpg', NULL, '2018-05-22 06:17:21', '2018-05-22 12:06:24', 7),
	(17, 'tristique in tempor id, sollicitudin id ligula.', 'tristique-in-tempor-id-sollicitudin-id-ligula', 'ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut ligula dolor. Curabitur varius non enim at elementum. Suspendisse potenti. Duis vel est semper, aliquam risus sit amet, tempor est. Donec nibh sapien, tristique in tempor id, sollicitudin id ligula. Aenean sapien dolor, tempus id facilisis non, scelerisque sit amet enim. Nunc dolor eros, placerat eu mauris ut, bibendum convallis elit. Proin enim felis, rhoncus id placerat ac, tempus et elit.', 6, '15269792661.jpg', NULL, '2018-05-22 08:54:26', '2018-05-22 08:54:26', 7),
	(18, 'Lorem ipsum dolor sit amet, consectetur adipiscing', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut ligula dolor. Curabitur varius non enim at elementum. Suspendisse potenti. Duis vel est semper, aliquam risus sit amet, tempor est. Donec nibh sapien, tristique in tempor id, sollicitudin id ligula. Aenean sapien dolor, tempus id facilisis non, scelerisque sit amet enim. Nunc dolor eros, placerat eu mauris ut, bibendum convallis elit. Proin enim felis, rhoncus id placerat ac, tempus et elit.\r\n\r\nUt sit amet iaculis dolor. Praesent placerat est quis est scelerisque, at lacinia magna fringilla. Vestibulum tincidunt neque diam, non tempus sapien varius sit amet. Fusce aliquet volutpat magna, quis accumsan tortor molestie sit amet. Nam sit amet tempus arcu. Morbi vel dictum mauris, a euismod diam. Donec cursus egestas justo. Nullam vehicula tincidunt diam, ac dapibus diam posuere eget. Sed at mauris mollis, egestas augue in, lacinia leo.', 5, '1526979322cricket.jpg', NULL, '2018-05-22 08:55:22', '2018-05-22 08:55:22', 8),
	(19, 'congue lacinia neque. Phasellus congue scelerisque', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing', 'efficitur dui vitae, congue lacinia neque. Phasellus congue scelerisque ante nec rutrum. Etiam bibendum eros turpis, a placerat lorem placerat ut. Vestibulum leo nunc, sollicitudin eu tincidunt ut, gravida id lacus. Nunc luctus rhoncus tortor, ut vehicula turpis congue quis. Morbi scelerisque mauris a turpis placerat, sed fringilla ante ullamcorper. Duis id gravida ante, non venenatis orci. Integer et cursus tortor. Fusce vitae mi cursus, ultricies neque ac, condimentum sapien. Cras elit leo, dapibus a ex non, venenatis semper urna.\r\n\r\nPhasellus odio neque, rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.', 5, '1527007793IMG20170207201017.jpg', '2018-05-22 18:21:25', '2018-05-22 16:49:53', '2018-05-22 18:21:25', 8),
	(20, 'Phasellus odio neque, rhoncus id accumsan id, element', 'phasellus-odio-neque-rhoncus-id-accumsan-id-element', 'rhoncus id accumsan id, elementum id justo. Pellentesque efficitur, tellus id dictum tincidunt, lectus ex finibus dolor, id elementum lacus felis id urna. Morbi sagittis dignissim mauris efficitur viverra. Pellentesque tristique consectetur dui, id tincidunt tellus consequat aliquet. Etiam nec tortor sagittis, condimentum tortor sit amet, sodales urna. Donec luctus odio libero, id posuere neque imperdiet id. Maecenas sed pharetra arcu. Nulla consectetur, ipsum vel tristique vehicula, nunc arcu pharetra arcu, id tincidunt nibh leo vitae elit. Sed tempus at augue at blandit. Cras non magna urna. Etiam dolor tellus, tincidunt sit amet nisi in, auctor commodo lacus. Donec blandit sed ante a pharetra. Aliquam consectetur ac diam non convallis. Fusce sed luctus nibh. Nullam a cursus massa.\r\n\r\nProin eu porta eros. Nam posuere facilisis euismod. Phasellus suscipit metus quis lectus condimentum,', 4, '1527013434IMG20170207201017.jpg', NULL, '2018-05-22 18:23:54', '2018-05-22 18:23:54', 7);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table blog.post_tag
CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.post_tag: ~51 rows (approximately)
DELETE FROM `post_tag`;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, NULL, NULL),
	(2, 2, 2, NULL, NULL),
	(3, 3, 1, NULL, NULL),
	(4, 4, 2, NULL, NULL),
	(5, 5, 2, NULL, NULL),
	(6, 6, 1, NULL, NULL),
	(7, 7, 2, NULL, NULL),
	(8, 8, 1, NULL, NULL),
	(9, 9, 1, NULL, NULL),
	(10, 10, 1, NULL, NULL),
	(11, 11, 2, NULL, NULL),
	(12, 12, 1, NULL, NULL),
	(13, 13, 1, NULL, NULL),
	(14, 14, 1, NULL, NULL),
	(15, 15, 1, NULL, NULL),
	(16, 16, 1, NULL, NULL),
	(17, 12, 2, NULL, NULL),
	(18, 2, 1, NULL, NULL),
	(19, 4, 1, NULL, NULL),
	(20, 13, 2, NULL, NULL),
	(21, 17, 7, NULL, NULL),
	(22, 18, 2, NULL, NULL),
	(23, 18, 5, NULL, NULL),
	(24, 18, 6, NULL, NULL),
	(25, 18, 7, NULL, NULL),
	(26, 16, 2, NULL, NULL),
	(27, 16, 4, NULL, NULL),
	(28, 16, 5, NULL, NULL),
	(29, 14, 2, NULL, NULL),
	(30, 14, 3, NULL, NULL),
	(31, 14, 4, NULL, NULL),
	(32, 14, 5, NULL, NULL),
	(33, 15, 3, NULL, NULL),
	(34, 15, 5, NULL, NULL),
	(35, 15, 6, NULL, NULL),
	(36, 15, 7, NULL, NULL),
	(37, 8, 2, NULL, NULL),
	(38, 8, 3, NULL, NULL),
	(39, 8, 4, NULL, NULL),
	(40, 8, 5, NULL, NULL),
	(41, 8, 6, NULL, NULL),
	(42, 8, 7, NULL, NULL),
	(43, 19, 1, NULL, NULL),
	(44, 19, 2, NULL, NULL),
	(45, 19, 4, NULL, NULL),
	(46, 19, 5, NULL, NULL),
	(47, 19, 7, NULL, NULL),
	(48, 20, 1, NULL, NULL),
	(49, 20, 3, NULL, NULL),
	(50, 20, 5, NULL, NULL),
	(51, 20, 6, NULL, NULL);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;

-- Dumping structure for table blog.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.profiles: ~5 rows (approximately)
DELETE FROM `profiles`;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` (`id`, `avatar`, `user_id`, `about`, `facebook`, `youtube`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/avatars/15267963770063719_exclusive-panjabi-for-men-lmp130.jpg', 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam earum error id in ipsa, ipsam placeat, possimus praesentium quae quibusdam quisquam repellat, suscipit. A ab iusto laborum nam perferendis, temporibus.', 'https://www.facebook.com/', 'https://www.youtube.com/', '2018-05-20 04:49:39', '2018-05-20 06:06:17'),
	(4, 'uploads/avatars/15263633151.jpg', 4, NULL, NULL, NULL, '2018-05-20 06:35:38', '2018-05-20 06:35:38'),
	(5, 'uploads/avatars/1527017822post-author2.png', 5, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam earum error id in ipsa, ipsam placeat, possimus praesentium quae quibusdam quisquam.', 'https://www.facebook.com/', 'https://www.youtube.com/', '2018-05-22 16:45:52', '2018-05-22 19:37:02'),
	(6, 'uploads/avatars/1527018467blog-details-author.png', 7, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam earum error id in ipsa, ipsam placeat, possimus praesentium quae quibusdam quisquam repellat,', 'https://www.facebook.com/', 'https://www.youtube.com/', '2018-05-22 19:44:05', '2018-05-22 19:47:47'),
	(7, 'uploads/avatars/1527019202reviews-avatar2.png', 8, 'কারণে-অকারণে অনেকেই আমাদের কাছে মিথ্যে বলে, পরে জানতে পেলে মনটাই খারাপ হয়ে যায়! কেমন হয় যদি তুমি সাথেসাথেই ধরে ফেলতে পারো ব্যাপারটা?', 'https://www.facebook.com/', 'https://www.youtube.com/', '2018-05-22 19:54:49', '2018-05-22 20:00:02');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;

-- Dumping structure for table blog.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.settings: ~0 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `site_name`, `contact_email`, `contact_number`, `address`, `created_at`, `updated_at`) VALUES
	(1, 'laravel\'s blog', 'habijabi@mail.org', '+880 1719034567', 'Dhaka, Bangladesh', '2018-05-20 09:54:45', '2018-05-21 08:45:23'),
	(2, 'W3codex', 'dhaka@mail.org', '+880 1719034567', 'Mirpur Dhaka', '2018-05-22 16:45:52', '2018-05-22 16:45:52');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table blog.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.tags: ~7 rows (approximately)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`id`, `tag`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'laravel', NULL, '2018-05-20 06:56:49', '2018-05-20 07:10:07'),
	(2, 'Blog', NULL, '2018-05-20 11:46:29', '2018-05-20 11:46:29'),
	(3, 'Wordpress', NULL, '2018-05-22 08:52:20', '2018-05-22 08:52:20'),
	(4, 'Python', NULL, '2018-05-22 08:52:42', '2018-05-22 08:52:42'),
	(5, 'tutorials', NULL, '2018-05-22 08:52:58', '2018-05-22 08:52:58'),
	(6, 'habi jabi', NULL, '2018-05-22 08:53:42', '2018-05-22 08:53:42'),
	(7, 'new kichu', NULL, '2018-05-22 08:53:52', '2018-05-22 08:53:52');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table blog.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `admin`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(7, 'raju sheikh', 'raju@mail.me', 1, '$2y$10$X3eRRT31ujOIKyMWUBkTdedxjLtcqT6VCNpynWYFbmWdNbYUsZkai', 'kXvJmIZC2TlpgRq51raX6s3OQ4HvCy5YvfbMuk60Mj3gaY2P6wN9js5nrvfm', '2018-05-22 19:44:05', '2018-05-22 19:47:48'),
	(8, 'আমি তুমি', 'habijabi@mail.com', 1, '$2y$10$SC4PaejfvAJBN99VtUrOSOeQ07h3vd6DSWANzuHjIiSVPlqAviG/u', NULL, '2018-05-22 19:54:49', '2018-05-22 20:01:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
