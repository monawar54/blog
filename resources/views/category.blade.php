

@extends('layouts.frontend')



@section('content')
    <div class="content-wrapper">
        <div class="stunning-header stunning-header-bg-lightviolet">
            <div class="stunning-header-content">
                <h1 class="stunning-header-title"> Category : {{ $category->name }}</h1>
            </div>
        </div>

        <!-- Post Details -->

        <div class="container">
            <div class="row medium-padding120">
                <main class="main">

                    <div class="row">
                        <div class="case-item-wrap">

                            @foreach($category->posts()->orderBy('id', 'desc')->take(9)->get() as $post)

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="case-item">
                                        <div class="case-item__thumb">
                                            <img class="category-img" src="{{ asset('uploads/posts/' . $post->featured) }}" alt="our case">
                                        </div>
                                        <h6 class="case-item__title"><a href="{{ route('post.single', ['slug' => $post->slug]) }}">{{ $post->title }}</a></h6>
                                    </div>
                                </div>

                            @endforeach


                        </div>
                    </div>

                    <!-- previous and next start -->

                    <div class="pagination-arrow">

                        @if($prev)
                            <a href="{{ route('category.single', ['id' => $prev->id]) }}" class="btn-prev-wrap">
                                <svg class="btn-prev">
                                    <use xlink:href="#arrow-left"></use>
                                </svg>
                                <div class="btn-content">
                                    <div class="btn-content-title">Previous Category</div>
                                    <p class="btn-content-subtitle">{{ $prev->category->name }}</p>
                                </div>
                            </a>
                        @endif

                        @if($next)
                            <a href="{{ route('category.single', ['id' => $next->id]) }}" class="btn-next-wrap">
                                <div class="btn-content">
                                    <div class="btn-content-title">Next Category</div>
                                    <p class="btn-content-subtitle">{{ $next->category->name }}</p>
                                </div>
                                <svg class="btn-next">
                                    <use xlink:href="#arrow-right"></use>
                                </svg>
                            </a>

                        @endif
                    </div>

                    <!-- previous and next End -->

                    <!-- End Post Details -->
                    <br>
                    <br>
                    <br>
                    <!-- Sidebar-->

                    <div class="col-lg-12">
                        <aside aria-label="sidebar" class="sidebar sidebar-right">
                            <div  class="widget w-tags">
                                <div class="heading text-center">
                                    <h4 class="heading-title">ALL BLOG TAGS</h4>
                                    <div class="heading-line">
                                        <span class="short-line"></span>
                                        <span class="long-line"></span>
                                    </div>
                                </div>

                                <div class="tags-wrap">
                                    <a href="#" class="w-tags-item">SEO</a>
                                    <a href="#" class="w-tags-item">Advertising</a>
                                    <a href="#" class="w-tags-item">Business</a>
                                    <a href="#" class="w-tags-item">Optimization</a>
                                    <a href="#" class="w-tags-item">Digital Marketing</a>
                                    <a href="#" class="w-tags-item">Social</a>
                                    <a href="#" class="w-tags-item">Keyword</a>
                                    <a href="#" class="w-tags-item">Strategy</a>
                                    <a href="#" class="w-tags-item">Audience</a>
                                </div>
                            </div>
                        </aside>
                    </div>

                    <!-- End Sidebar-->

                </main>
            </div>
        </div>

    </div>
@endsection