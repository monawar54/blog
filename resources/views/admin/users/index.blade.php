

@extends('layouts.app')

@section('content')

    @if(Session::has('info'))
        <div class="alert alert-info" role="alert">
            {{ Session::get('info') }}
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">
            Users
        </div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th> Sl.</th>
                <th> Image </th>
                <th> Name </th>
                <th> permission </th>
                <th class="text-center"> Action </th>
            </tr>
            </thead>

            <tbody>
            @if ($users->count() > 0)
                @php $i=1; @endphp
                @foreach($users as $user)
                    <tr>
                        <td> {{ $i++ }}</td>
                        <td>
                            <img width="70" height="60" src=" {{ asset($user->profile->avatar) }}" alt=" {{ $user->name }}" style="border-radius: 50%;" >
                        </td>
                        <td> {{ $user->name }}</td>
                        <td>
                            @if($user->admin)
                                <a href="{{ route('user.not.admin', ['$id'=>$user->id]) }}" class="btn btn-xs btn-danger">Remove permission</a>
                                @else
                                <a href="{{ route('user.admin', ['$id'=>$user->id]) }}" class="btn btn-xs btn-success">Make admin</a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('user.delete', ['$id'=>$user->id]) }}" class="btn btn-xs btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            @else

                <tr>
                    <th colspan="5" class="text-center"> No users. </th>
                </tr>

            @endif

            </tbody>
        </table>
    </div>

@endsection