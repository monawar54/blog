
@extends('layouts.app')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">
            Category List
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th> Sl.</th>
                    <th> Category Name </th>
                    <th> Action </th>
                </tr>
            </thead>

            <tbody>
            @if ($categories->count() > 0)
                @php $i=1; @endphp
                @foreach($categories as $category)
                    <tr>
                        <td> {{ $i++ }}</td>
                        <td> {{ $category->name }}</td>
                        <td>

                            <a href=" {{ route('category.edit', ['id' => $category->id]) }}" class="btn btn-xs btn-info" >Edit</a>
                            <a href=" {{ route('category.delete', ['id' => $category->id]) }}" class="btn btn-xs btn-danger" >Delete</a>

                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <th colspan="5" class="text-center"> No categories post yet. </th>
                </tr>
            @endif

            </tbody>
        </table>
    </div>

@endsection