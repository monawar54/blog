
@extends('layouts.app')

@section('content')

    @include('admin.includes.errors')


    <div class="card card-default">
        <div class="card-header">
           Edit blog settings
        </div>

        <div class="card-body">
            <form action="{{ route('settings.update') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name"><strong>Site Name</strong></label>
                    <input type="text" name="site_name" value="{{ $settings->site_name }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email"><strong>Contact Email</strong></label>
                    <input type="email" name="contact_email" value="{{ $settings->contact_email }}" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="email"><strong>Contact Number</strong></label>
                    <input type="text" name="contact_number" value="{{ $settings->contact_number }}" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="email"><strong>Address</strong></label>
                    <input type="text" name="address" value="{{ $settings->address }}" class="form-control" >
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">
                            Update Settings
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection