

@extends('layouts.app')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">
            Trashed List
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th> Sl.</th>
                    <th> Tag </th>
                    <th > Action </th>
                </tr>
            </thead>

            <tbody>
            @if ($tags->count() > 0)
                @php $i=1; @endphp
                @foreach($tags as $tag)
                    <tr>
                        <td> {{ $i++ }}</td>
                        <td> {{ $tag->tag }}</td>
                        <td>
                            <a href=" {{ route('tag.restore', ['id' => $tag->id]) }}" class="btn btn-xs btn-success"> Restore</a>
                            <a href=" {{ route('tag.kill', ['id' => $tag->id]) }}" class="btn btn-xs btn-danger"> Delete</a>

                        </td>
                    </tr>
                @endforeach
            @else

                <tr>
                    <th colspan="5" class="text-center"> No trashed tag yet. </th>
                </tr>

            @endif

            </tbody>
        </table>
    </div>

@endsection