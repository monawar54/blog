

@extends('layouts.app')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="card card-default">
        <div class="card-header">
            Trashed List
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th> Sl.</th>
                    <th> Title </th>
                    <th> Image </th>
                    <th> Content </th>
                    <th class="text-center"> Action </th>
                </tr>
            </thead>

            <tbody>
            @if ($posts->count() > 0)
                @php $i=1; @endphp
                @foreach($posts as $post)
                    <tr>
                        <td> {{ $i++ }}</td>
                        <td> {{ substr($post->title,0,10) }}</td>
                        <td>
                            <img width="90" height="50" src=" {{ asset('uploads/posts/' . $post->featured) }}" alt=" {{ $post->title }}">
                        </td>
                        <td> {{ substr($post->content,0,20) }}</td>
                        <td>
                            <a href=" {{ route('post.restore', ['id' => $post->id]) }}" class="btn btn-xs btn-success"> Restore</a>
                            <a href=" {{ route('post.kill', ['id' => $post->id]) }}" class="btn btn-xs btn-danger"> Delete</a>

                        </td>
                    </tr>
                @endforeach
            @else

                <tr>
                    <th colspan="5" class="text-center"> No trashed post yet. </th>
                </tr>

            @endif

            </tbody>
        </table>
    </div>

@endsection