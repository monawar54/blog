<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//use Newsletter;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

Route::get('/test', function (){
    return App\Profile::find(1)->user;
});

Route::get('/', [
    'uses' => 'FrontEndController@index',
    'as' => 'index'
]);

Route::get('/results', function () {
    $posts = \App\Post::where('title', 'like', '%' . request('query') . '%')->get();

    return view('results')->with('posts', $posts)
                                ->with('title', 'Search results :' . request('query'))
                                ->with('tags', \App\Tag::all())
                                ->with('settings', \App\Setting::first())
                                ->with('categories', \App\Category::take(5)->get())
                                ->with('query', request('query'));
});

Route::post('/subscribe', function () {
    $email = request('email');

    Newsletter::subscribe($email);

    Session::flash('subscribed', 'successfully subscribed.');

    return redirect()->back();
});



Route::get('/post/{slug}',[
    'uses'=>'FrontEndController@single_post',
    'as' => 'post.single'
]);

Route::get('/category/{sid}',[
    'uses'=>'FrontEndController@category',
    'as' => 'category.single'
]);

Route::get('/tag/{id}', [
    'uses' => 'FrontEndController@tag',
    'as' => 'tag.single'
]);

//Route::get('/home', 'HomeController@index');
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()
{

    Route::get('/home', [
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);

    // post module for blog

    Route::get('/post/create', [
        'uses' => 'PostController@create',
        'as'  => 'post.create'
    ]);

    Route::post('/post/store', [
        'uses' => 'PostController@store',
        'as'  => 'post.store'
    ]);

    Route::get('/post/index', [
        'uses' => 'PostController@index',
        'as'  => 'post.index'
    ]);

    Route::get('/post/edit/{id}', [
        'uses' => 'PostController@edit',
        'as'  => 'post.edit'
    ]);

    Route::post('/post/update/{id}', [
        'uses' => 'PostController@update',
        'as'  => 'post.update'
    ]);


    Route::get('/post/trashed', [
        'uses' => 'PostController@trashed',
        'as'  => 'post.trashed'
    ]);

    Route::get('/post/delete/{id}', [
        'uses' => 'PostController@destroy',
        'as'  => 'post.delete'
    ]);

    Route::get('/post/kill/{id}', [
        'uses' => 'PostController@kill',
        'as'  => 'post.kill'
    ]);

    Route::get('/post/restore/{id}', [
        'uses' => 'PostController@restore',
        'as'  => 'post.restore'
    ]);

    // category module for blog

    Route::get('/category/create', [
        'uses' => 'CategoriesController@create',
        'as'  => 'category.create'
    ]);

    Route::post('/category/store', [
        'uses' => 'CategoriesController@store',
        'as'  => 'category.store'
    ]);

    Route::get('/category/index', [
        'uses' => 'CategoriesController@index',
        'as'  => 'category.index'
    ]);

    Route::get('/category/edit/{id}', [
        'uses' => 'CategoriesController@edit',
        'as'  => 'category.edit'
    ]);

    Route::get('/category/delete/{id}', [
        'uses' => 'CategoriesController@destroy',
        'as'  => 'category.delete'
    ]);

    Route::post('/category/update/{id}', [
        'uses' => 'CategoriesController@update',
        'as'  => 'category.update'
    ]);


    // tag module for blog

    Route::get('/tag/create', [
        'uses' => 'TagsController@create',
        'as'  => 'tag.create'
    ]);

    Route::get('/tag/index', [
        'uses' => 'TagsController@index',
        'as'  => 'tag.index'
    ]);

    Route::post('/tag/store', [
        'uses' => 'TagsController@store',
        'as'  => 'tag.store'
    ]);

    Route::get('/tag/edit/{id}', [
        'uses' => 'TagsController@edit',
        'as'  => 'tag.edit'
    ]);

    Route::post('/tag/update/{id}', [
        'uses' => 'TagsController@update',
        'as'  => 'tag.update'
    ]);

    Route::get('/tag/delete/{id}', [
        'uses' => 'TagsController@destroy',
        'as'  => 'tag.delete'
    ]);

    Route::get('/tag/trashed', [
        'uses' => 'TagsController@trashed',
        'as'  => 'tag.trashed'
    ]);

    Route::get('/tag/kill/{id}', [
        'uses' => 'TagsController@kill',
        'as'  => 'tag.kill'
    ]);

    Route::get('/tag/restore/{id}', [
        'uses' => 'TagsController@restore',
        'as'  => 'tag.restore'
    ]);

    Route::get('/users', [
        'uses' => 'UsersController@index',
        'as' => 'users'
    ]);

    Route::get('/user/create', [
        'uses' => 'UsersController@create',
        'as' => 'user.create'
    ]);

    Route::post('/user/store', [
        'uses' => 'UsersController@store',
        'as' => 'user.store'
    ]);

    Route::get('/user/delete/{id}', [
        'uses' => 'UsersController@destroy',
        'as' => 'user.delete'
    ]);

    Route::get('/user/admin/{id}', [
        'uses' => 'UsersController@admin',
        'as' => 'user.admin'
    ]);

    Route::get('/user/not-admin/{id}', [
        'uses' => 'UsersController@not_admin',
        'as' => 'user.not.admin'
    ]);

    Route::get('/user/profile', [
        'uses' => 'ProfilesController@index',
        'as' => 'user.profile'
    ]);

    Route::post('/user/profile/update', [
        'uses' => 'ProfilesController@update',
        'as' => 'user.profile.update'
    ]);

    Route::get('/settings', [
        'uses' => 'SettingsController@index',
        'as' => 'settings'
    ]);

   /* Route::get('/settings/edit', [
        'uses' => 'SettingsController@index',
        'as' => 'settings.edit'
    ]);*/

    Route::post('/settings/update', [
        'uses' => 'SettingsController@update',
        'as' => 'settings.update'
    ]);




});


Auth::routes();


