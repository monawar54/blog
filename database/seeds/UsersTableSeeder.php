<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'raju sheikh',
            'email' => 'raju@mail.me',
            'password' => bcrypt('password'),
            'admin' => 1
        ]);

        App\Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/15263633151.jpg',
            'about' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam earum error id in ipsa, ipsam placeat, possimus praesentium quae quibusdam quisquam repellat.',
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com'
         ]);

    }

}
