<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Setting::create([

            'site_name' => 'W3codex',
            'contact_email' => 'dhaka@mail.org',
            'contact_number' => '+880 1719034567',
            'address' => 'Mirpur Dhaka'

        ]);
    }
}
