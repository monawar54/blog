<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Tag;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tags.index')->with('tags', Tag::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*if ($tags->count() == 0)
        {
            Session::flash('success', 'You must add a tag before attempting to create a post');
        }*/
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        Tag::create([

            'tag' => $request->tag,

        ]);

        Session::flash('success', 'Tag Created Successfully.');

        return redirect()->route('tag.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);

        return view('admin.tags.edit')->with('tag', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        $tag = Tag::find($id);

        $tag->tag = $request->tag;
        $tag->save();

        Session::flash('success', 'Tag Updated successfully');

        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::destroy($id);

        Session::flash('success', 'Tag trashed successfully.');

        return redirect()->back();
    }



    public function trashed()
    {
        $tags = Tag::onlyTrashed()->get();

        return view('admin.tags.trashed')->with('tags', $tags);
    }




    public function kill($id)
    {
        $tag = Tag::withTrashed()->where('id', $id)->first();
        $tag->forceDelete();

        Session::flush('success', 'Tag permanently Deleted.');

        return redirect()->route('tag.trashed');
    }




    public function restore($id)
    {
        $tag = Tag::withTrashed()->where('id', $id)->first();
        $tag->restore();

        Session::flash('success', 'Tag Restore Successfully.');

        return redirect()->route('tag.index');
    }
}
