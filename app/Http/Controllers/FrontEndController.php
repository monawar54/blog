<?php

namespace App\Http\Controllers;

use App\Post;
use App\Setting;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function index()
    {

        return view('index')
            ->with('title', Setting::first()->site_name)
            ->with('categories', Category::take(5)->get())
            ->with('first_post', Post::orderBy('created_at', 'desc')->first())
            ->with('second_post', Post::orderBy('created_at', 'desc')->skip(1)->take(1)->get()->first())
            ->with('third_post', Post::orderBy('created_at', 'desc')->skip(2)->take(1)->get()->first())
            ->with('tutorials', Category::find(4))
            ->with('laravel',Category::find(3))
            ->with('blog', Category::find(5))
            ->with('settings', Setting::first());
    }


    public function single_post($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $next_id = Post::where('id', '>', $post->id)->min('id');
        $prev_id = Post::where('id', '<', $post->id)->max('id');

    return view('single')->with('post', $post)
                                ->with('title', $post->title)
                                ->with('settings', Setting::first())
                                ->with('categories', Category::take(5)->get())
                                ->with('tags', Tag::all())
                                ->with('next', Post::find($next_id))
                                ->with('prev', Post::find($prev_id));

    }

    public function category($id)
    {
        $category = Category::find($id);

        $next_id = Post::where('id', '>', $category->id)->min('id'); //for next post or anything
        $prev_id = Post::where('id', '<', $category->id)->max('id');  //for previous post or anything

        return view('category')->with('category', $category)
            ->with('title', $category->name)
            ->with('settings', Setting::first())
            ->with('categories', Category::take(5)->get())
            ->with('next', Post::find($next_id)) //for next button
            ->with('prev', Post::find($prev_id)); //for previous button
    }

    public function tag($id)
    {
        $tag = Tag::find($id);

        return view('tag')->with('tag', $tag)
                                ->with('title', $tag->tag)
                                ->with('settings', Setting::first())
                                ->with('categories', Category::take(5)->get())
                                ->with('tags', Tag::all());
    }

}
